/*
 * CLOCK.c
 *
 * Created: 1/3/2016 7:45:34 PM
 *  Author: Andre
 */ 


#include "CLOCK.h"
#include <avr/io.h>
#include "xmegaDriver/clksys_driver.h"

void initExternal32MhzClock()
{		
     /* konfiguriere Taktquelle */
     CLKSYS_XOSC_Config(OSC_FRQRANGE_12TO16_gc,false,OSC_XOSCSEL_XTAL_16KCLK_gc);
     /* aktiviere Taktquelle */
     CLKSYS_Enable( OSC_XOSCEN_bm );
     /* konfiguriere PLL, Taktquelle, Faktor */
     CLKSYS_PLL_Config( OSC_PLLSRC_XOSC_gc, 2 ); 
     /* aktiviere PLL */
     CLKSYS_Enable( OSC_PLLEN_bm );
     /* konfiguriere Prescaler */
     CLKSYS_Prescalers_Config( CLK_PSADIV_1_gc, CLK_PSBCDIV_1_1_gc );
     /* warte bis takt stabil */ 
     do {} while ( CLKSYS_IsReady( OSC_PLLRDY_bm ) == 0 );
     /* w�hle neue Taktquelle */
     CLKSYS_Main_ClockSource_Select( CLK_SCLKSEL_PLL_gc );
     /* deaktiviere internen Oszillator */
     CLKSYS_Disable( OSC_XOSCEN_bm );	 
}

void initInternal32MhzClock(void)
{
   OSC.CTRL |= OSC_RC32MEN_bm;
   while(!(OSC.STATUS & OSC_RC32MRDY_bm));
   CCP = CCP_IOREG_gc;
   CLK.CTRL = CLK_SCLKSEL_RC32M_gc;
}