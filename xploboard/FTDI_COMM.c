/*
 * FTDI_COMM.c
 *
 * Created: 1/3/2016 7:14:37 PM
 *  Author: Andre
 */ 

#include "FTDI_COMM.h"


#define CARRIAGE_RETURN 0x0D
#define LINE_FEED       0x0A

/************************************************************************/
/*                                                                      */
/************************************************************************/

#define CONTROLB(x) x##_BAUDCTRLB
#define CONTROLA(x) x##_BAUDCTRLA


/************************************************************************/
/* UART set baudrate                                                    */
/************************************************************************/

static void setBaudrate(USART_t *usart, USART_BAUDRATE baudrate)
{
    usart->BAUDCTRLB = (-3<<USART_BSCALE0_bp) |((131>>8) << USART_BSEL0_bp);
    usart->BAUDCTRLA = (131<<USART_BSEL0_bp);
}


void setupUart(PORT_t *_port, USART_t *usart, uint8_t usartIdx, UART_HDLR *hdlr, USART_BAUDRATE baudrate)
{
	/*
	 * Init the FTDI UART with 115200 kBaud / s
	 */
	
	if(hdlr){
		setBaudrate(usart, baudrate);
	
	    usart->CTRLA = USART_RXCINTLVL_LO_gc;	        //Set receive interupt as high
	    usart->CTRLB = USART_TXEN_bm | USART_RXEN_bm;	//Enable receive and transmit
	    usart->CTRLC = USART_CHSIZE_8BIT_gc;	        //Set mode to 8 Bit
	    if(usartIdx == 1){
            _port->DIR |= PIN7_bm;
	    }
	    else{
		    _port->DIR |= PIN3_bm;
	    }	
	}
	
	hdlr->baudrate = baudrate;
	hdlr->dev = usart;
	hdlr->usartPort = _port;
}



/************************************************************************/
/* Wait until data register is empty									*/
/************************************************************************/

static inline void waitForUsartDataRegisterEmpty(UART_HDLR *hdlr)
{
	while(!(hdlr->dev->STATUS & USART_DREIF_bm));
}


/************************************************************************/
/* Wait for received sign									*/
/************************************************************************/

static inline void waitForUsartReceiveFlag(UART_HDLR *hdlr)
{
	while(!(hdlr->dev->STATUS & USART_RXCIF_bm));
}


/************************************************************************/
/* Send character array over uart										*/
/************************************************************************/
int sendCharArrayOverUart(uint8_t data[], UART_HDLR *hdlr)
{
	int cntr = 0x00;
	int length = 0x00;
	
	length = strlen((char*)data);  //Get length of data stream
	
	//Send complete data stream
	while(cntr < length)
	{	
		waitForUsartDataRegisterEmpty(hdlr);
		hdlr->dev->DATA = data[cntr];					//transmit sign into send register
		cntr++;
	}
	
	cntr = 0;
	
	waitForUsartDataRegisterEmpty(hdlr);
	hdlr->dev->DATA = LINE_FEED;
	waitForUsartDataRegisterEmpty(hdlr);
	hdlr->dev->DATA = CARRIAGE_RETURN;
	
	return 0;	
}

void sendCharSignOverUart(char data, UART_HDLR *hdlr)
{
	waitForUsartDataRegisterEmpty(hdlr);
	hdlr->dev->DATA = data;					//transmit sign into send register
}