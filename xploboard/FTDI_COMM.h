/*
 * FTDI_COMM.h
 *
 * Created: 1/3/2016 7:14:47 PM
 *  Author: Andre
 */ 


#ifndef FTDI_COMM_H_
#define FTDI_COMM_H_

#include <avr/io.h>

typedef enum{
	BAUDRATE_9600 = 0,
	BAUDRATE_19200,  
	BAUDRATE_115200	
}USART_BAUDRATE;

typedef struct{
	USART_t *dev;
	USART_BAUDRATE baudrate;
	PORT_t * usartPort;
}UART_HDLR;


/***********************************************************************
 Setup USART
 @param: _port        //Used port for UART communication
 @param: usart        //Used UART device (e.g. USARTD0)
 @param: usartIdx     //Used Index like 1 = USARTD1 / 0 = USARTD0
 @param: hdlr         //Structure with saved informations
 @param: baudrate     //Selected baudrate defined by USART_BAUDRATE enum
************************************************************************/


void setupUart(PORT_t *_port, USART_t *usart, uint8_t usartIdx, UART_HDLR *hdlr, USART_BAUDRATE baudrate);

/************************************************************************/
/* Send string                                                          */
/************************************************************************/

int sendCharArrayOverUart(uint8_t data[], UART_HDLR *hdlr);

void sendCharSignOverUart(char data, UART_HDLR *hdlr);


#endif /* FTDI_COMM_H_ */