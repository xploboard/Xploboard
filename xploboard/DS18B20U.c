/*
 * DS18B20U.c
 *
 * Created: 1/4/2016 3:13:05 PM
 *  Author: Andre
 */ 

#include "DS18B20U.h"
#include <avr/delay.h>

#define F_CPU       32000000
#define LOOP_CYCLES 4

#define us(x)       (x/(LOOP_CYCLES*(1/(F_CPU/1000000.0))))

inline __attribute__((gnu_inline)) void therm_delay(uint16_t delay){
	while(delay--) asm volatile ("nop");
}


void therm_delay_sec(uint16_t sec){
	
	uint16_t idx= 1000;
	
	while(sec--) 
	{
        while(idx--)
		    therm_delay(us(1000));	
	}		
}


//#define DEBUG

#ifdef DEBUG
    #define DS_PORT     PORTC
#else
    #define DS_PORT     PORTA
#endif
	    
#define DS_DQ_PIN       PIN6_bm
#define DQ_HIGH()       DS_PORT.OUT |= DS_DQ_PIN
#define DQ_LOW()        DS_PORT.OUT &= ~DS_DQ_PIN
#define DQ_OUTPUT()     DS_PORT.DIR |= DS_DQ_PIN
#define DQ_INPUT()      DS_PORT.DIR &= ~DS_DQ_PIN
#define DQ_STATE        (DS_PORT.IN & DS_DQ_PIN)

#define THERM_DECIMAL_12BIT_STEPS   625 //.625
/************************************************************************/
/* DS18B20 reset pulse                                                  */
/************************************************************************/
DS18B20_STATE resetTempSense()
{
	DS18B20_STATE state;
	DQ_OUTPUT();
	DQ_LOW();
	
	//wait 480 ms
	therm_delay(us(480));
	
	DQ_INPUT();
	
	therm_delay(us(60));
	
	state = DQ_STATE;
	
	therm_delay(us(420));
		
	return state;
}

/************************************************************************/
/* Writes bit (one or zero) to bus                                      */
/************************************************************************/
void writeBit(uint8_t bit)
{
	DQ_OUTPUT();
	DQ_LOW() ;
	
	therm_delay(us(1));
	
	if(bit)
	    DQ_INPUT();
	
	therm_delay(us(60));
	
	DQ_INPUT();	
}


void writeByte(uint8_t data)
{
	uint8_t i = 8;
	
	while(i--)
	{
		writeBit(data & 0x01);
		
		data = data >> 1;
	}
}

/************************************************************************/
/* Reads bit (one or zero) from bus                                      */
/************************************************************************/
uint8_t readBit()
{
	volatile uint8_t bit;


	DQ_LOW() ;
		DQ_OUTPUT();
	
	therm_delay(us(1));
	
	DQ_INPUT();
	
	therm_delay(us(14));
	
	bit = PORTA.IN & PIN6_bm ;
	
	therm_delay(us(45));
	
	if(bit)
	    return 0x01;
	
	return 0;
}


uint8_t readByte()
{
	volatile uint8_t i=8, n=0;
	
	while(i--)
	{
		n >>= 1;
		n |= (readBit() << 7);
	}
	
	return n;
}


/************************************************************************/
/* DS18B20: Read temperature                                            */
/************************************************************************/
void readTemperature(DS18B20_TEMP *temp)
{
	volatile uint8_t t[2];
	
	resetTempSense();
	writeByte(SKIP_ROM);
	writeByte(CONVERT_TEMP);
	
	while(!readBit());
	
	PORTC.DIRSET = PIN7_bm;
	PORTC.OUTSET = PIN7_bm;
	
	resetTempSense();
	writeByte(SKIP_ROM);
	writeByte(READ_SCRATCH);
	
	t[0] = readByte();
	t[1] = readByte();
	
	resetTempSense();
	
	temp->digit = t[0] >> 4;
	temp->digit |= (t[1]&0x7) << 4;
	
	temp->decimal = t[0] & 0xf;
	temp->decimal *=THERM_DECIMAL_12BIT_STEPS;	
}