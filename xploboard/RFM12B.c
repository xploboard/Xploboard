/*
 * RFM12B.c
 *
 * Created: 1/6/2016 3:15:12 PM
 *  Author: Andre
 */ 

#include "RFM12B.h"

/************************************************************************/
/* Init RFM and SPI                                                     */
/************************************************************************/

#define ASSERT_CHIPSELECT()	    RFM_PORT.OUTCLR = RFM_NSEL
#define DEASSERT_CHIPSELECT()   RFM_PORT.OUTSET = RFM_NSEL

void initRfmSpi()
{	
	//configure mosi, cs, clk as output
	RFM_PORT.DIRSET =   RFM_SCK | RFM_SDI | RFM_NSEL;
	RFM_PORT.DIRCLR =   RFM_SDO | RFM_NIRQ;
	
    RFM_SPI.CTRL = SPI_ENABLE_bm | SPI_MASTER_bm | SPI_MODE_0_gc | SPI_PRESCALER_DIV16_gc;
	
	RFM_PORT.INTCTRL = SPI_INTLVL_OFF_gc;
}

uint16_t rfmWriteCommand(uint8_t preamble, uint8_t data)
{ 
	volatile uint16_t val;  
	ASSERT_CHIPSELECT();
	
	RFM_SPI.DATA = preamble;
	while(!(RFM_SPI.STATUS & (1<<7)));
	val = RFM_SPI.DATA;
	val <<= 8;
	
	RFM_SPI.DATA = data;	
	while(!(RFM_SPI.STATUS & (1<<7)));
	val |= RFM_SPI.DATA;
		
	DEASSERT_CHIPSELECT();
	
	return val;
}

#define SPI_STATUS      rfmWriteCommand(0x00,0x00)
#define RFM_RGIT        (SPI_STATUS & 0x8000)
#define WAIT_RFM_RGIT()   while(!RFM_RGIT)

#define CONFIG_COMMANDS
#define CONFIG_SETTING  

#ifdef TRANSMIT_MODE
    #define POWERMANAGEMET      0x82,0x08   
	#define POWERMANAGEMET_OFF  0x82,0x08   //Power down moule
#else   //Receiver
    #define POWERMANAGEMET      0x82,0xD8   
	#define POWERMANAGEMET_OFF  0x82,0x08   //Power down moule
#endif

#define FREQUENCY           0xA6,0x7C   //868 + 352*0,005 MHZ = 869,76 MHZ
#define DATARATE            0xC6,0x05   //4,8 kbps
#define RECEIVER_CONTROL    0x94,0xA0   //VDI en, FAST response, 134 khz BW, 0dBm, -103 dBm threshold
#define DATA_FILTER         0xC2,0xAC   //Clock recovery auto lock, clock recovery fast mode, digital filter, dqd4
#define FIFO_MODE           0xCA,0xF1   //FIFO8,SYNC,!ff,DR 
#define SYNC_PATTTERN       0xCE,0xD4   //SYNC = 2DD4
#define AFC_COMMAND         0xC4,0x83   //AFC offset when vdi high, no restriction, enable output, enable afc
#define TX_CONF             0x98,0x10   //30kHz deviaton, max power
#define PLL_SETTING         0xCC,0x77   //OB1?OB0,?lpx,?ddy?DDIT?BW0 
#define WAKE_UP_TIMER       0xE0,0x00   //Wake up timer disabled
#define LOW_DUTY_CYCLE      0xC8,0x00   //Disabled

#define WRITE_DATA_TO_TX    0xB8


/************************************************************************/
/* Powermanagement functions (Power on / off)                           */
/************************************************************************/
void rfmPowerOn()
{
	rfmWriteCommand(0x00,0x00);
	rfmWriteCommand(POWERMANAGEMET);
}

void rfmPowerOff()
{
	rfmWriteCommand(POWERMANAGEMET_OFF);
}


void initRfm12b(void)
{
	//Init SPI
	initRfmSpi();
	
    rfmWriteCommand(0x80,0xE7); //EL,EF,868band,12.0pF
#ifdef TRANSMIT_MODE
    rfmWriteCommand(0x82, 0x39); //!er,!ebb,ET,ES,EX,!eb,!ew,DC
#else	
    rfmWriteCommand(0x82, 0x99); //er,!ebb,ET,ES,EX,!eb,!ew,DC (bug was here)
#endif
	rfmWriteCommand(0xA6, 0x40); //freq select
    rfmWriteCommand(0xC6, 0x47); //4.8kbps
    rfmWriteCommand(0x94, 0xA0); //VDI,FAST,134kHz,0dBm,-103dBm
    rfmWriteCommand(0xC2, 0xAC); //AL,!ml,DIG,DQD4
    rfmWriteCommand(0xCA, 0x81); //FIFO8,SYNC,!ff,DR (FIFO level = 8)
    rfmWriteCommand(0xCE, 0xD4); //SYNC=2DD4;
    rfmWriteCommand(0xC4, 0x83); //@PWR,NO RSTRIC,!st,!fi,OE,EN
    rfmWriteCommand(0x98, 0x50); //!mp,90kHz,MAX OUT
    rfmWriteCommand(0xCC, 0x17); //!OB1,!OB0, LPX,!ddy,DDIT,BW0
    rfmWriteCommand(0xE0, 0x00); //NOT USE
    rfmWriteCommand(0xC8, 0x00); //NOT USE
    rfmWriteCommand(0xC0, 0x40); //1.66MHz,2.2V
}

/************************************************************************/
/* Polling nIrq until latest transmit is over                           */
/************************************************************************/
static inline void waitForTransmitReady()
{
	while(RFM_PORT.IN & RFM_NIRQ){}
}

/************************************************************************/
/* Polling nIRQ                                                         */
/************************************************************************/
void waitForReceiveReady()
{
	while(RFM_PORT.IN & RFM_NIRQ){}
}

/************************************************************************/
/* Reset FIFO for receiver ready                                        */
/************************************************************************/

void resetFIFO()
{
    rfmWriteCommand(0xCA,0x81);	
    rfmWriteCommand(0xCA,0x83);
}

/************************************************************************/
/* rfm read byte                                                        */
/************************************************************************/

uint8_t rfmReadByte()
{
	uint16_t data;
	
	WAIT_RFM_RGIT();
	
	data = rfmWriteCommand(0xB0, 0x00);
	
	return (data&0x00FF);	
}

/************************************************************************/
/* Writing byte to transmit buffer                                      */
/************************************************************************/

void rfmTransmitByte(uint8_t dataByte)
{
	waitForTransmitReady();
	rfmWriteCommand(WRITE_DATA_TO_TX, dataByte);
}

void rfmReadStatus()
{
	volatile uint8_t state = 0x00;
	rfmWriteCommand(state, state);
}