/*
 * DS18B20U.h
 *
 * Created: 1/4/2016 3:12:55 PM
 *  Author: Andre
 */ 


#ifndef DS18B20U_H_
#define DS18B20U_H_


#include <avr/io.h>

typedef enum DS18B20_STATE {
	DS18B20_OK = 0,
	DS18B20_NOK
}DS18B20_STATE;

typedef struct DS18B20_TEMP {
	int8_t digit;
	uint16_t decimal;
}DS18B20_TEMP;

/************************************************************************/
/* DS18B20 Codes                                                        */
/************************************************************************/
#define SEARCH_ROM      0xf0
#define READ_ROM        0x33
#define MATCH_ROM       0x55
#define SKIP_ROM        0xcc
#define ALARM_SEARCH    0xec

#define CONVERT_TEMP    0x44
#define WRITE_SCRATCH   0x4e
#define READ_SCRATCH    0xbe
#define COPY_SCRATCH    0x48
#define RECALL_EEPROM   0xb8
#define READ_POWER_SUPP 0xb4


/************************************************************************/
/* DS18B20 reset pulse                                                  */
/************************************************************************/
DS18B20_STATE resetTempSense();


/************************************************************************/
/* DS18B20: Read temperature                                            */
/************************************************************************/
void readTemperature(DS18B20_TEMP *temp);



void therm_delay_sec(uint16_t sec);

#endif /* DS18B20U_H_ */