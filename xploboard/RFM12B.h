/*
 * RFM12B.h
 *
 * Created: 1/6/2016 3:15:23 PM
 *  Author: Andre
 */ 


#ifndef RFM12B_H_
#define RFM12B_H_

#include <avr/io.h>


//Port & Pin definition
#define RFM_PORT    PORTE
#define RFM_SPI     SPIE

#define RFM_SCK     PIN7_bm
#define RFM_SDO     PIN6_bm
#define RFM_SDI     PIN5_bm
#define RFM_NIRQ    PIN3_bm
#define RFM_NSEL    PIN4_bm

#define WAIT_NIRQ_LOW   while((RFM_PORT.IN & RFM_NIRQ))

//#define TRANSMIT_MODE

/************************************************************************/
/* Init RFM and SPI                                                     */
/************************************************************************/

void initRfm12b(void);
/************************************************************************/
/* Powermanagement functions (Power on / off)                           */
/************************************************************************/
void rfmPowerOn();

void rfmPowerOff();

/************************************************************************/
/* Writing byte to transmit buffer                                      */
/************************************************************************/

void rfmTransmitByte(uint8_t dataByte);

uint16_t rfmWriteCommand(uint8_t preamble, uint8_t data);

/************************************************************************/
/* Polling nIRQ                                                         */
/************************************************************************/
void waitForReceiveReady();

/************************************************************************/
/* rfm read byte                                                        */
/************************************************************************/

uint8_t rfmReadByte();

/************************************************************************/
/* Reset FIFO for receiver ready                                        */
/************************************************************************/

void resetFIFO();


#endif /* RFM12B_H_ */