/*
 * OLED.h
 *
 * Created: 9/23/2015 12:29:55 PM
 *  Author: Andre
 */ 


#ifndef OLED_H_
#define OLED_H_


#include <avr/io.h>

/************************************************************************/
//  OLED Pinout
//  SPI F used
//  PF3 = D/C   (LOW = Command, HIGH = Data)
//  PF4 = Chip Select
//  PF5 = Display data in (MOSI)
//  PF7 = SCK
/************************************************************************/

//Used xploboard port
#define OLED_PORT   PORTD
#define OLED_SPI    SPID

#define RESET       PIN2_bm
#define D_C         PIN3_bm
#define CS          PIN4_bm
#define SCK         PIN7_bm
#define MOSI        PIN5_bm

//Macro functions
#define ASSERT_CHIPSELECT()	    OLED_PORT.OUTCLR = CS
#define DEASSERT_CHIPSELECT()   OLED_PORT.OUTSET = CS

#define ASSERT_RESET()          OLED_PORT.OUTCLR = RESET
#define DEASSERT_RESET()        OLED_PORT.OUTSET = RESET
   
#define SEL_DATA()              OLED_PORT.OUTSET = D_C
#define SEL_COMMAND()           OLED_PORT.OUTCLR = D_C


#define    RED  0x0000FF
#define  GREEN  0x00FF00
#define   BLUE  0xFF0000
#define  WHITE  0xFFFFFF
#define  BLACK  0x000000

/************************************************************************/
/* Data & Command functions                                             */
/************************************************************************/

void oledWriteData(uint8_t data);

void oledWriteCommand(uint8_t command);

/************************************************************************/
/* Initialization                                                       */
/************************************************************************/

void initOled();

void showSpectrum(void);                  // function to show color spectrum

void OLED_FillScreen_128128RGB(unsigned long color);


void OLED_Text_128128RGB(unsigned char x_pos, unsigned char y_pos, unsigned char letter, unsigned long textColor, unsigned long backgroundColor);

uint8_t oledWriteText(uint8_t line, uint8_t pos, uint8_t* text, uint8_t txtLength, unsigned long textColor, unsigned long backgroundColor);

void startScrolling();
#endif /* OLED_H_ */