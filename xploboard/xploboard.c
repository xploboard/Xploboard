/*
 xploboard.c
 Created: 1/3/2016 7:02:59 PM
 Author: Andre
 
 Measuring temperature and show it on OLED display
 */ 


#include <avr/io.h>
#include "FTDI_COMM.h"
#include "CLOCK.h"
#include "DS18B20U.h"
#include "OLED.h"
#include "RFM12B.h"

static void testRfmTransmit();

int main(void)
{
	DS18B20_TEMP temp;
	char buffer[32];
	
	initExternal32MhzClock();
	
	initOled();
	
	OLED_FillScreen_128128RGB(BLACK);

    oledWriteText(1,1,"XPLOBOARD2", 10, WHITE, BLACK);
	
	/************************************************************************/
	/* Init rfm12b transmitter                                              */
	/************************************************************************/
    initRfm12b();
#ifdef TRANSMIT_MODE
    testRfmTransmit();
#else	
	testRfmReceive();
#endif

	while(1)
    {
		
		readTemperature(&temp);

        sprintf(buffer, "Temp: %+d.%04u�C", temp.digit, temp.decimal);
	
	    oledWriteText(2, 1, buffer,16, WHITE, BLACK);
		
        therm_delay_sec(1);
		
		//TODO:: Please write your application code 
    }
}

static void testRfmTransmit()
{
	volatile uint8_t index = 0;
	
	_delay_ms(25);
	
    rfmWriteCommand(0x00,0x00);
	rfmTransmitByte(0xAA);
	rfmTransmitByte(0xAA);
	rfmTransmitByte(0xAA);
	rfmTransmitByte(0x2D);
	rfmTransmitByte(0xD4);
	
	for(index = 0; index < 16; index ++){
		
	}
	rfmTransmitByte('J');
	rfmTransmitByte('A');
	rfmTransmitByte('N');
	rfmTransmitByte('I');
	rfmTransmitByte('N');
	rfmTransmitByte('E');
	
	rfmTransmitByte(0xAA);
	rfmTransmitByte(0xAA);
	rfmTransmitByte(0xAA);
}

void testRfmReceive()
{
	volatile uint8_t i = 0;
	uint8_t recArr[6];
	_delay_ms(25);
	
	resetFIFO();

    while(1)
	{
		while(!(RFM_PORT.IN & RFM_NIRQ))
		{
	        
	        for(uint8_t j = 0; j < 6; j ++){
				i = rfmReadByte();
				recArr[j] = i;
			}				
				
			    oledWriteText(3, 1, recArr,6, WHITE, BLACK);
				
				return;			
		}
    }	    			

}