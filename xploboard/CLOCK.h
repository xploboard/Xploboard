/*
 * CLOCK.h
 *
 * Created: 1/3/2016 7:45:42 PM
 *  Author: Andre
 */ 


#ifndef CLOCK_H_
#define CLOCK_H_


#ifndef F_CPU
#define F_CPU 32000000UL
#endif

#include <util/delay.h>
	
/************************************************************************/
/* Initializing Clockhardware to 2 or 32 MHZ                            */
/************************************************************************/

void initExternal32MhzClock();

void initInternal32MhzClock(void);



#endif /* CLOCK_H_ */